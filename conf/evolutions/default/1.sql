# User schema

# --- !Ups
create table `USERS` (
  `ID` INT NOT NULL AUTO_INCREMENT PRIMARY KEY,
  `FIRST_NAME` TEXT NOT NULL,
  `LAST_NAME` TEXT NOT NULL,
  `EMAIL` TEXT NOT NULL,
  `PASSWORD` TEXT NOT NULL
)

# --- !Downs
drop table `user`