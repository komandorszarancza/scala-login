import {Injectable} from "@angular/core";
import {HttpClient, HttpHeaders} from "@angular/common/http";
import {Observable} from "rxjs";
import {User} from "../models/user.model";
import {TokenService} from "./token.service";

@Injectable()
export class UserService {
  private serviceUrl = '/api/user';

  httpOptions = {
    headers: new HttpHeaders({
      'Content-Type': 'application/json',
      'User': this.token.get()
    })
  };

  constructor(private http: HttpClient, private token: TokenService) {
  }

  create(user: User): Observable<User> {
    return this.http.post<User>(this.serviceUrl, user, this.httpOptions);
  }

  update(user: User) {
    return this.http.put(this.serviceUrl, user, this.httpOptions);
  }

  delete(): Observable<any> {
    return this.http.delete('/api/user', this.httpOptions)
  }

  get(id: string): Observable<User> {
    return this.http.get<User>(`${this.serviceUrl}/${id}`, this.httpOptions);
  }

  getProfile(): Observable<User> {
    return this.http.get<User>('/api/profile', this.httpOptions)
  }

  getAll(): Observable<Array<User>> {
    return this.http.get<Array<User>>(`/api/users`, this.httpOptions);
  }

  changePassword(model: object): Observable<any> {
    return this.http.post("/api/changePassword", model, this.httpOptions);
  }

}
