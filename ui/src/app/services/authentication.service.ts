import {Injectable} from "@angular/core";
import {HttpClient, HttpHeaders} from "@angular/common/http";
import {BasicAuth} from "../models/basic-auth.model";
import {Observable} from "rxjs";

@Injectable()
export class AuthenticationService {
  private serviceUrl = '/api/login';

  private httpOptions = {
    headers: new HttpHeaders({
      'Content-Type': 'application/json'
    })
  };

  constructor(private http: HttpClient) {
  }

  login(model: BasicAuth): Observable<any> {
   return this.http.post(this.serviceUrl,model,this.httpOptions);
  }

  resetPassword(model: object): Observable<any> {
    return this.http.post("/api/resetPassword", model);
  }
}
