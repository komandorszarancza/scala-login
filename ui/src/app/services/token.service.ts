import {Injectable} from "@angular/core";
import {HttpClient} from "@angular/common/http";
import {BehaviorSubject, Observable} from "rxjs";


@Injectable()
export class TokenService {
  private key = 'scala-login-key';
  private loggedIn = new BehaviorSubject<boolean>(false);

  constructor(private http: HttpClient) {
  }

  save(token: string) {
    localStorage.setItem(this.key, token);
    this.loggedIn.next(true);
  }

  get isLoggedIn() {
    return this.loggedIn.asObservable(); // {2}
  }

  get(): string {
    return localStorage.getItem(this.key);
  }

  check(): Observable<any>{
    return this.http.post("/api/checkToken", {token: this.get()})
  }

  clear() {                            // {4}
    this.loggedIn.next(false);
    localStorage.setItem(this.key, "")
  }

}
