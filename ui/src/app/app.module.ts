import {BrowserModule} from '@angular/platform-browser';
import {NgModule} from '@angular/core';

import {AppComponent} from './components/app/app.component';
import {HTTP_INTERCEPTORS, HttpClientModule} from '@angular/common/http';
import RouteConfig from './config/routes.config';
import {RouteExampleComponent} from './route-example/route-example.component';
import {HomeComponent} from './components/home/home.component';
import {LoginComponent} from './components/login/login.component';
import {RegisterComponent} from './components/register/register.component';
import {NavigationComponent} from './components/navigation/navigation.component';
import {ReactiveFormsModule} from "@angular/forms";
import {UserService} from "./services/user.service";
import {UserListComponent} from './components/home/user-list/user-list.component';
import {ProfileComponent} from './components/profile/profile.component';
import {AuthenticationService} from "./services/authentication.service";
import {TokenService} from "./services/token.service";
import {ChangePasswordComponent} from './components/change-password/change-password.component';
import {ResetPasswordComponent} from './components/reset-password/reset-password.component';
import {MatSnackBarModule} from "@angular/material";
import {HttpErrorInterceptor} from "./interceptor/http.interceptor";
import {BrowserAnimationsModule} from "@angular/platform-browser/animations";
import {EditComponent} from './components/edit/edit.component';

@NgModule({
  declarations: [
    AppComponent,
    RouteExampleComponent,
    HomeComponent,
    LoginComponent,
    RegisterComponent,
    NavigationComponent,
    UserListComponent,
    ProfileComponent,
    ChangePasswordComponent,
    ResetPasswordComponent,
    EditComponent
  ],
  imports: [
    BrowserModule,
    HttpClientModule,
    RouteConfig,
    ReactiveFormsModule,
    BrowserAnimationsModule,
    MatSnackBarModule
  ],
  providers: [
    UserService,
    AuthenticationService,
    TokenService,
    {
      provide: HTTP_INTERCEPTORS, useClass: HttpErrorInterceptor, multi: true
    }
  ],
  bootstrap: [AppComponent]
})
export class AppModule {
}
