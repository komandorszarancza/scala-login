export interface BasicAuth {
  email: string;
  password: string;
}
