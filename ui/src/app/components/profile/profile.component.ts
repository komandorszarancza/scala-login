import {Component, OnInit} from '@angular/core';
import {User} from "../../models/user.model";
import {ActivatedRoute, ParamMap, Router} from "@angular/router";
import {switchMap} from "rxjs/operators";
import {UserService} from "../../services/user.service";

@Component({
  selector: 'app-profile',
  templateUrl: './profile.component.html',
  styleUrls: ['./profile.component.css']
})
export class ProfileComponent implements OnInit {
  user: User;


  constructor(private router: Router,
              private route: ActivatedRoute,
              private userSerivce: UserService) { }

  ngOnInit() {
    const response = this.route.paramMap.pipe(
      switchMap((params: ParamMap) => this.userSerivce.get(params.get('id')))
    );
    response.subscribe(data => {
      if (data) {
        this.user = data
      } else {
        this.router.navigate([''])
      }
    });
  }

}
