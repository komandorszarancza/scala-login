import {Component, OnInit} from '@angular/core';
import {FormBuilder, Validators} from "@angular/forms";
import {AuthenticationService} from "../../services/authentication.service";
import {TokenService} from "../../services/token.service";
import {Router} from "@angular/router";

@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.css']
})
export class LoginComponent implements OnInit {

  loginForm = this.formBuilder.group({
    email: ['', [Validators.required ,Validators.email]],
    password: ['', [
      Validators.required,
      Validators.minLength(5),
      Validators.pattern('^(?=.*[a-z])(?=.*[A-Z])(?=.*[@$!%*?&])[A-Za-z\\d@$!%*?&]{5,10}$')]]
  });


  constructor(private formBuilder: FormBuilder,
              private authService: AuthenticationService,
              private token: TokenService,
              private router: Router) { }

  ngOnInit() {
  }

  onSubmit() {
    this.authService.login(this.loginForm.value).subscribe(response => {
      this.token.save(response.message);
      this.router.navigate([''])
    })
  }

}
