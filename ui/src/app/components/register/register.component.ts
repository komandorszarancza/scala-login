import {Component, OnInit} from '@angular/core';
import {FormBuilder, Validators} from "@angular/forms";
import {UserService} from "../../services/user.service";
import {Router} from "@angular/router";
import {environment} from "../../../environments/environment";

@Component({
  selector: 'app-register',
  templateUrl: './register.component.html',
  styleUrls: ['./register.component.css']
})
export class RegisterComponent implements OnInit {

  registerForm = this.formBuilder.group({
    id: [0],
    firstName: ['',[Validators.required, Validators.minLength(1)]],
    lastName: ['',[Validators.required, Validators.minLength(1)]],
    email: ['', [Validators.required ,Validators.email]],
    password: ['', [
      Validators.required,
      Validators.minLength(1),
      Validators.pattern(environment.email)]
    ]
  });

  constructor(private formBuilder: FormBuilder, private userService: UserService, private router: Router) { }

  ngOnInit() {
  }

  onSubmit() {
    this.userService.create(this.registerForm.value).subscribe(response => {
      this.router.navigate([''])
    })
  }
}
