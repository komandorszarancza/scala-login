import {Component, OnInit} from '@angular/core';
import {UserService} from "../../../services/user.service";
import {User} from "../../../models/user.model";
import {Router} from "@angular/router";

@Component({
  selector: 'app-user-list',
  templateUrl: './user-list.component.html',
  styleUrls: ['./user-list.component.css']
})
export class UserListComponent implements OnInit {
  userList: Array<User>;

  constructor(private userService: UserService, private router: Router) { }

  ngOnInit() {
    this.userService.getAll().subscribe(response => {
      this.userList = response;
      console.log(this.userList);
    })
  }

  onSelectedUser(id: string) {
    this.router.navigate(['profile',id]);
  }

}
