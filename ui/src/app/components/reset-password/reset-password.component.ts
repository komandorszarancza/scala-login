import {Component, OnInit} from '@angular/core';
import {FormBuilder, Validators} from "@angular/forms";
import {AuthenticationService} from "../../services/authentication.service";
import {MatSnackBar} from "@angular/material";
import {Router} from "@angular/router";

@Component({
  selector: 'app-reset-password',
  templateUrl: './reset-password.component.html',
  styleUrls: ['./reset-password.component.css']
})
export class ResetPasswordComponent implements OnInit {

  form = this.formBuilder.group({
    email: ['', [Validators.required ,Validators.email]]
  });

  constructor(private formBuilder: FormBuilder,
              private service: AuthenticationService,
              private snackBar: MatSnackBar,
              private  router: Router) { }

  ngOnInit() {
  }

  onSubmit() {
    this.service.resetPassword(this.form.value).subscribe(() => {
      this.snackBar.open("Email with new password was send", "CLOSE", {duration: 1500});
      this.router.navigate(["/login"]);
    })
  }

}
