import {Component, OnInit} from '@angular/core';
import {FormBuilder, Validators} from "@angular/forms";
import {UserService} from "../../services/user.service";
import {Router} from "@angular/router";
import {User} from "../../models/user.model";

@Component({
  selector: 'app-edit',
  templateUrl: './edit.component.html',
  styleUrls: ['./edit.component.css']
})
export class EditComponent implements OnInit {

  form = this.formBuilder.group({
    id: [0],
    firstName: ['',[Validators.required, Validators.minLength(1)]],
    lastName: ['',[Validators.required, Validators.minLength(1)]],
    email: ['', [Validators.required ,Validators.email]],
    password: ['']
  });

  constructor(private formBuilder: FormBuilder, private userService: UserService, private router: Router) { }

  ngOnInit() {
    this.userService.getProfile().subscribe((data: User) => {
      this.form.setValue({
        id: data.id,
        firstName: data.firstName,
        lastName: data.lastName,
        email: data.email,
      })
    })
  }

  onSubmit() {
    this.userService.update(this.form.value).subscribe(() => {
      this.router.navigate([''])
    })
  }

}
