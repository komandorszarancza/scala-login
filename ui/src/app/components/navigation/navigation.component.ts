import {Component, OnInit} from '@angular/core';
import {TokenService} from "../../services/token.service";
import {Router} from "@angular/router";
import {UserService} from "../../services/user.service";

@Component({
  selector: 'app-navigation',
  templateUrl: './navigation.component.html',
  styleUrls: ['./navigation.component.css']
})
export class NavigationComponent implements OnInit {

  isLoggedIn = false;

  constructor(private userService: UserService, private service: TokenService, private router: Router) {
  }

  ngOnInit() {
    this.service.isLoggedIn.subscribe(value => this.isLoggedIn = value)
    if (this.service.get().length > 0) {
      this.service.save(this.service.get());
    } else {
      this.service.clear();
    }
  }

  onLogout() {
    this.service.clear();
    this.router.navigate([''])
  }

  onDelete() {
    this.userService.delete().subscribe(() => this.onLogout())
  }

}
