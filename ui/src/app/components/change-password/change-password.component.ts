import {Component, OnInit} from '@angular/core';
import {FormBuilder, Validators} from "@angular/forms";
import {UserService} from "../../services/user.service";
import {Router} from "@angular/router";
import {MatSnackBar} from "@angular/material";
import {environment} from "../../../environments/environment";

@Component({
  selector: 'app-change-password',
  templateUrl: './change-password.component.html',
  styleUrls: ['./change-password.component.css']
})
export class ChangePasswordComponent implements OnInit {

  form = this.formBuilder.group({
    email: ['', [Validators.required, Validators.email, Validators.minLength(1)]],
    oldPassword: ['', [
      Validators.required,
      Validators.minLength(1),
      Validators.pattern(environment.email)]
    ],
    password: ['', [
      Validators.required,
      Validators.minLength(1),
      Validators.pattern(environment.email)]
    ],
    repeatedPassword: ['', [
      Validators.required,
      Validators.minLength(1),
      Validators.pattern(environment.email)]
    ]
  });

  constructor(private formBuilder: FormBuilder, private userService: UserService, private router: Router, private snackBar: MatSnackBar) {
  }

  ngOnInit() {
  }

  onSubmit() {
    if (this.form.valid) {
      this.userService.changePassword(this.form.value).subscribe((data) => {
        this.snackBar.open(data.message, environment.snackBarButton, {duration: environment.snackBarDuration})
        this.router.navigate([''])
      })
    }
  }
}
