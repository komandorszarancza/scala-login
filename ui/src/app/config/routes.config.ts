import {RouterModule, Routes} from "@angular/router";
import {HomeComponent} from "../components/home/home.component";
import {LoginComponent} from "../components/login/login.component";
import {RegisterComponent} from "../components/register/register.component";
import {ProfileComponent} from "../components/profile/profile.component";
import {ResetPasswordComponent} from "../components/reset-password/reset-password.component";
import {ChangePasswordComponent} from "../components/change-password/change-password.component";
import {AuthGuard} from "../guard/auth-guard.service";
import {EditComponent} from "../components/edit/edit.component";

const routes: Routes = [
  {
    path: '',
    component: HomeComponent
  },
  {
    path: 'login',
    component: LoginComponent
  },
  {
    path: 'register',
    component: RegisterComponent
  },
  {
    path: 'profile/:id',
    component: ProfileComponent
  },
  {
    path: 'reset-password',
    component: ResetPasswordComponent
  },
  {
    path: 'change-password',
    component: ChangePasswordComponent,
    canActivate: [AuthGuard]
  },
  {
    path: 'edit',
    component: EditComponent,
    canActivate: [AuthGuard]
  },
  {
    path: '**',
    redirectTo: ''
  }
];

export default RouterModule.forRoot(routes);
