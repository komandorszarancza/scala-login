import sbt.Keys.libraryDependencies

name := """scala-login"""

version := "1.0-SNAPSHOT"

lazy val login = (project in file(".")).enablePlugins(PlayJava).settings(
  watchSources ++= (baseDirectory.value / "public/ui" ** "*").get
)

resolvers += Resolver.sonatypeRepo("snapshots")

scalaVersion := "2.12.2"

libraryDependencies ++= Seq(
  guice,
  "org.scalatestplus.play" %% "scalatestplus-play" % "3.1.2" % Test,
  "com.typesafe.play" %% "play-slick" % "3.0.0",
  "com.typesafe.play" %% "play-slick-evolutions" % "3.0.0",
  "com.typesafe.play" %% "play-mailer" % "6.0.1",
  "com.typesafe.play" %% "play-mailer-guice" % "6.0.1",
  "com.jason-goodwin" %% "authentikat-jwt" % "0.4.5",
  "org.mindrot" % "jbcrypt" % "0.4",
  "com.h2database" % "h2" % "1.4.197"
)

