package utils

import org.mindrot.jbcrypt.BCrypt

object HashUtils {
  def getHash(str: String) : String = {
    BCrypt.hashpw(str, BCrypt.gensalt())
  }

  def checkHash(str: String, strHashed: String): Boolean = {
    BCrypt.checkpw(str,strHashed)
  }

}
