package services

import javax.inject._
import models.{User, Users}

import scala.concurrent.Future

@Singleton
class UserService @Inject()(users: Users) {

  def addUser(user: User): Future[String] = {
    users.add(user)
  }

  def deleteUser(id: Long): Future[String] = {
    users.delete(id)
  }

  def updateUser(user: User): Future[String] = {
    users.update(user)
  }

  def getUser(id: Long): Future[Option[User]] = {
    users.get(id)
  }

  def getUser(email: String, password: String): Future[Option[User]] = {
    users.get(email, password)
  }

  def getUser(email: String): Future[Option[User]] = {
    users.get(email)
  }

  def listAllUsers: Future[Seq[User]] = {
    users.listAll
  }
}