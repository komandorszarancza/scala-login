package services

import javax.inject.{Inject, Singleton}
import models.{Address, Addresses}

import scala.concurrent.Future

@Singleton
class AdressService @Inject() (adresses: Addresses) {

  def save(address: Address): Future[String] = {
    adresses.add(address)
  }

  def update(address: Address): Future[String] = {
    adresses.update(address)
  }

  def delete(id: Long): Future[Int] = {
    adresses.delete(id)
  }

  def get(id: Long): Future[Option[Address]] = {
    adresses.get(id)
  }

  def getForUser(id: Long): Future[Seq[Address]] = {
    adresses.listFor(id)
  }

}
