package services

import javax.inject.Inject
import play.api.libs.mailer
import play.api.libs.mailer.MailerClient

class MailService @Inject()(mailService: MailerClient) {

  def resetPassword(email: String, password: String): Unit = {
    val message = mailer.Email(
      "Password Reset Request",
      "komandor@o2.pl",
      Seq(email),
      bodyText = Some(password))
    mailService.send(message)
  }
}
