package actions

import javax.inject.Inject
import models.UserRequest
import play.api.mvc._
import services.UserService
import utils.JwtUtility

import scala.concurrent.{Future, _}

class AuthAction @Inject()(bodyParser: BodyParsers.Default, userService: UserService)(implicit ec: ExecutionContext) extends ActionBuilder[UserRequest, AnyContent] {

  override def parser: BodyParser[AnyContent] = bodyParser

  override protected def executionContext: ExecutionContext = ec

  override def invokeBlock[A](request: Request[A], block: UserRequest[A] => Future[Result]): Future[Result] = {
    val jwtToken = request.headers.get("User").getOrElse("")
    println(jwtToken)
    if (JwtUtility.isValidToken(jwtToken)) {
      JwtUtility.decodePayload(jwtToken).fold {
        Future.successful(Results.Unauthorized("Invalid credential"))
      } { payload =>
        val userId = payload.toInt
        println(userId)
        userService.getUser(userId) flatMap {
          case Some(i) => block(UserRequest(i, request))
          case None => Future.successful(Results.Unauthorized("Invalid credential"))
        }
      }
    } else {
      Future.successful(Results.Unauthorized("Invalid credential"))
    }
  }
}