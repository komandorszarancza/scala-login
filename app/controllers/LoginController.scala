package controllers

import actions.AuthAction
import javax.inject._
import models.{BasicAuth, Response, User}
import play.api.libs.json._
import play.api.mvc._
import services.{MailService, UserService}
import utils.{HashUtils, JwtUtility}

import scala.concurrent.ExecutionContext.Implicits.global
import scala.concurrent.Future

@Singleton
class LoginController @Inject()(jwtAction: AuthAction, mailService: MailService, userService: UserService, cc: ControllerComponents) extends AbstractController(cc) {

  def login: Action[AnyContent] = Action.async { implicit request =>
    val json = request.body.asJson.get
    val credentials = json.as[BasicAuth]
    println(credentials)
    println(HashUtils.getHash(credentials.password))
    userService.getUser(credentials.email) flatMap { user: Option[User] =>
      if (user.isDefined) {
        val tempUser = user.get
        if (HashUtils.checkHash(credentials.password, tempUser.password)) {
          Future(Ok(Response.ok(JwtUtility.createToken(tempUser.id.toString))))
        } else {
          Future(Results.BadRequest("Wrong Password"))
        }
      } else {
        Future(Results.BadRequest("User does not Exist"))
      }
    }
  }

  def resetPassword: Action[JsValue] = Action(parse.json).async { request: Request[JsValue] =>
    val email = (request.body \ "email").as[String]
    val password = randomString(8)
    val ps = "A" + password + "!!"
    try {
      mailService.resetPassword(email, ps)
      userService.getUser(email) flatMap { user: Option[User] =>
        if (user.isDefined) {
          val temp = user.get.copy(password = ps)
          userService.updateUser(temp) flatMap { res =>
            Future(Ok(Response.ok(res)))
          }
        } else {
          Future(Results.BadRequest("User with provided email does not exist"))
        }
      }
    } catch {
      case ex: Exception => {
        Future(Results.RequestTimeout("Error occured while sending mail"))
      }
    }
  }

  def checkToken: Action[JsValue] = Action(parse.json).async { request: Request[JsValue] =>
    val token = (request.body \ "token").as[String]
    println(JwtUtility.isValidToken(token))
    if(JwtUtility.isValidToken(token)) {
      Future(Ok)
    } else {
      Future(BadRequest("Unauthorized access"))
    }
  }

  private def randomString(length: Int): String = {
    val r = new scala.util.Random
    val sb = new StringBuilder
    for (i <- 1 to length) {
      sb.append(r.nextPrintableChar)
    }
    sb.toString
  }
}
