package controllers

import actions.AuthAction
import javax.inject._
import models.{ChangePassword, Response, User}
import play.api.libs.json._
import play.api.mvc._
import services.UserService
import utils.{HashUtils, ValidationUtils}

import scala.concurrent.ExecutionContext.Implicits.global
import scala.concurrent.Future

@Singleton
class UserController @Inject()(jwtAction: AuthAction, userService: UserService, cc: ControllerComponents) extends AbstractController(cc) {

  def add: Action[AnyContent] = Action.async { implicit request =>
    val json = request.body.asJson.get
    val newUser = json.as[User]
    println(newUser)
    if (!ValidationUtils.validatePassword(newUser.password)) {
      Future(Results.BadRequest("Bad Password"))
    } else if (!ValidationUtils.validateEmail(newUser.email)) {
      Future(Results.BadRequest("Bad Email"))
    } else {
      userService.getUser(newUser.email) flatMap { user: Option[User] =>
        if (user.isDefined) {
          Future(Results.BadRequest("Email Exists"))
        } else {
          println(newUser)
          val u = newUser.copy(password = HashUtils.getHash(newUser.password))
          userService.addUser(u) flatMap { res =>
            Future(Ok(Response.ok(res)))
          }
        }
      }
    }
  }

  def list: Action[AnyContent] = Action.async { implicit request =>
    userService.listAllUsers map { users =>
      Ok(Json.toJson(users))
    }
  }

  def get(id: Long): Action[AnyContent] = Action.async { implicit request =>
    println(id)
    userService.getUser(id) map { user =>
      println(user)
      Ok(Json.toJson(user))
    }
  }

  def delete: Action[AnyContent] = jwtAction.async { implicit request =>
    val model = request.user
    userService.deleteUser(model.id) map { msg =>
      Ok(Response.ok(msg))
    }
  }

  def getProfile: Action[AnyContent] = jwtAction.async { implicit request =>
    val model = request.user
    userService.getUser(model.id) map { user =>
      Ok(Json.toJson(user))
    }
  }

  def update: Action[AnyContent] = jwtAction.async { implicit request =>
    val json = request.body.asJson.get
    val tempUser = json.as[User]
    if (!ValidationUtils.validateEmail(tempUser.email)) {
      Future(Results.BadRequest("Bad Email"))
    } else {
      userService.getUser(tempUser.id) flatMap { user: Option[User] =>
        if (user.isDefined) {
          val temp = user.get
          if (temp.email != tempUser.email) {
            userService.getUser(tempUser.email) flatMap { user2: Option[User] =>
              if (user2.isDefined) {
                Future(Results.BadRequest("Email Exists"))
              } else {
                val userWithNewPassword = tempUser.copy(password = user.get.password)
                userService.updateUser(userWithNewPassword) flatMap { res =>
                  Future(Ok(Response.ok(res)))
                }
              }
            }
          } else {
            val updatedUser = tempUser.copy(password = user.get.password)
            userService.updateUser(updatedUser) flatMap { res =>
              Future(Ok(Response.ok(res)))
            }
          }
        } else {
          Future(Results.BadRequest("User does not exists"))
        }
      }
    }
  }

  def changePassword: Action[AnyContent] = jwtAction.async { implicit request =>
    val json = request.body.asJson.get
    val model = json.as[ChangePassword]
    if (model.oldPassword == model.password) {
      Future(Results.BadRequest("Old and new Password is the same"))
    } else if (model.password != model.repeatedPassword) {
      Future(Results.BadRequest("New Password and repeated Password does not match"))
    } else {
      userService.getUser(model.email) flatMap { user: Option[User] =>
        if (user.isDefined) {
          val temp = user.get
          if (HashUtils.checkHash(model.password, temp.password)) {
            Future(Results.BadRequest("New password is the same as current password"))
          } else {
            val u = temp.copy(password = HashUtils.getHash(model.password))
            Future(Ok(Response.ok("Password update successful")))
          }
        } else {
          Future(Results.BadRequest("Incorrect email"))
        }
      }
    }
  }
}
