package controllers

import actions.AuthAction
import javax.inject._
import models.{Address, Response}
import play.api.libs.json._
import play.api.mvc._
import services.AdressService

import scala.concurrent.ExecutionContext.Implicits.global
import scala.concurrent.Future

@Singleton
class AddressController @Inject()(jwtAction: AuthAction, service: AdressService, cc: ControllerComponents) extends AbstractController(cc) {

  def save: Action[AnyContent] = jwtAction.async  { implicit request =>
    val json = request.body.asJson.get
    val model = json.as[Address]
    if (model.title.isEmpty) {
      Future(Results.BadRequest("Title is empty"))
    } else if(model.street.isEmpty) {
      Future(Results.BadRequest("Street is empty"))
    } else if(model.city.isEmpty) {
      Future(Results.BadRequest("City is empty"))
    } else {
      service.save(model.copy(userId = request.user.id)) flatMap { r =>
        Future(Ok(Response.ok(r)))
      }
    }
  }


  def update: Action[AnyContent] = jwtAction.async  { implicit request =>
    val json = request.body.asJson.get
    val model = json.as[Address]
    if (model.title.isEmpty) {
      Future(Results.BadRequest("Title is empty"))
    } else if(model.street.isEmpty) {
      Future(Results.BadRequest("Street is empty"))
    } else if(model.city.isEmpty) {
      Future(Results.BadRequest("City is empty"))
    } else {
      service.update(model) flatMap { msg =>
        Future(Ok(Response.ok(msg)))
      }
    }
  }

  def delete(id: Long): Action[AnyContent] = jwtAction.async  { implicit request =>
    service.delete(id) map { msg =>
      Ok(Response.ok(msg.toString))
    }
  }

  def get(id: Long): Action[AnyContent] = Action.async { implicit request =>
    service.get(id) flatMap { address: Option[Address] =>
      Future(Ok(Json.toJson(address)))
    }
  }

  def getAddresses(id: Long): Action[AnyContent] = Action.async  { implicit request =>
    service.getForUser(id) flatMap { addressList: Seq[Address] =>
      Future(Ok(Json.toJson(addressList)))
    }
  }
}
