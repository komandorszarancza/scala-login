package models

import play.api.libs.json.{JsObject, Json}

object Response {
  def error(message: String): JsObject = {
    Json.obj("error" -> message)
  }

  def ok(message: String): JsObject = {
    Json.obj("message" -> message)
  }
}
