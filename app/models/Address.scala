package models

import javax.inject.{Inject, _}
import play.api.db.slick.{DatabaseConfigProvider, HasDatabaseConfigProvider}
import play.api.libs.functional.syntax._
import play.api.libs.json.{JsPath, Reads, Writes, _}
import slick.driver.H2Driver.api._
import slick.driver.JdbcProfile

import scala.concurrent.ExecutionContext.Implicits.global
import scala.concurrent.Future

case class Address(id: Long, userId: Long, title: String, street: String, city: String, number: Long, code: String)

object Address {
  implicit val userWrites: Writes[Address] = Writes {
    address => Json.obj (
      "id" -> address.id,
      "title" -> address.title,
      "street" -> address.street,
      "city" -> address.city,
      "number" -> address.number,
      "code" -> address.code
    )
  }

  implicit val userReads: Reads[Address] = (
    (JsPath \ "id").read[Long] and
    (JsPath \ "userId").read[Long] and
      (JsPath \ "title").read[String] and
      (JsPath \ "street").read[String] and
      (JsPath \ "city").read[String] and
      (JsPath \ "number").read[Long] and
      (JsPath \ "code").read[String]
    ) (Address.apply _)
}

class AddressTableDef(tag: Tag) extends Table[Address](tag, "ADRESSES") {
  def id = column[Long]("ID", O.PrimaryKey, O.AutoInc)

  def userId = column[Long]("USER_ID")

  def title = column[String]("TITLE")

  def street = column[String]("STREET")

  def city = column[String]("CITY")

  def code = column[String]("CODE")

  def number = column[Long]("NUMBER")

  override def * =
    (id, userId, title, street, city, number, code)  <> ((Address.apply _).tupled, Address.unapply)
}

@Singleton
class Addresses @Inject()(protected val dbConfigProvider: DatabaseConfigProvider) extends HasDatabaseConfigProvider[JdbcProfile] {

  val addreses = TableQuery[AddressTableDef]

  def add(address: Address): Future[String] = {
    db.run(addreses += address).map(_ => "Success")
  }

  def delete(id: Long): Future[Int] = {
    db.run(addreses.filter(_.id === id).delete)
  }

  def update(address: Address): Future[String] = {
    db.run(addreses.insertOrUpdate(address)).map(_ => "Success")
  }

  def listFor(id: Long): Future[Seq[Address]] = {
    db.run(addreses.filter(_.userId === id).result)
  }

  def get(id: Long): Future[Option[Address]] = {
    db.run(addreses.filter(_.id === id).result.headOption)
  }

}
