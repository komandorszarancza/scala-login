package models

import play.api.libs.functional.syntax.{unlift, _}
import play.api.libs.json.Reads._
import play.api.libs.json.{JsPath, Reads, Writes}

case class ChangePassword(email: String, oldPassword: String, password: String, repeatedPassword: String)

object ChangePassword {
  implicit val passwordhWrites: Writes[ChangePassword] = (
    (JsPath \ "email").write[String] and
      (JsPath \ "oldPassword").write[String] and
      (JsPath \ "password").write[String] and
      (JsPath \ "repeatedPassword").write[String]
    ) (unlift(ChangePassword.unapply))

  implicit val passwordReads: Reads[ChangePassword] = (
    (JsPath \ "email").read[String] and
      (JsPath \ "oldPassword").read[String] and
      (JsPath \ "password").read[String] and
      (JsPath \ "repeatedPassword").read[String]
    ) (ChangePassword.apply _)
}
