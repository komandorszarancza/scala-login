package models

import play.api.libs.functional.syntax.{unlift, _}
import play.api.libs.json.Reads._
import play.api.libs.json.{JsPath, Reads, Writes}

case class BasicAuth(email: String, password: String)

object BasicAuth {
  implicit val authWrites: Writes[BasicAuth] = (
    (JsPath \ "email").write[String] and
      (JsPath \ "password").write[String]
    )(unlift(BasicAuth.unapply))

  implicit val authReads: Reads[BasicAuth] = (
    (JsPath \ "email").read[String] and
      (JsPath \ "password").read[String]
    )(BasicAuth.apply _)
}
