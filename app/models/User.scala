package models


import javax.inject.{Inject, _}
import play.api.db.slick.{DatabaseConfigProvider, HasDatabaseConfigProvider}
import play.api.libs.functional.syntax._
import play.api.libs.json.{JsPath, Reads, Writes, _}
import slick.jdbc.H2Profile.api._
import slick.jdbc.JdbcProfile

import scala.concurrent.ExecutionContext.Implicits.global
import scala.concurrent.Future

case class User(id: Long, firstName: String, lastName: String, email: String, password: String)

object User {
  implicit val userWrites: Writes[User] = Writes {
    user => Json.obj (
      "id" -> user.id,
      "firstName" -> user.firstName,
      "lastName" -> user.lastName,
      "email" -> user.email
    )
  }

  implicit val userReads: Reads[User] = (
    (JsPath \ "id").read[Long] and
      (JsPath \ "firstName").read[String] and
      (JsPath \ "lastName").read[String] and
      (JsPath \ "email").read[String] and
      (JsPath \ "password").read[String]
    ) (User.apply _)
}

class UserTableDef(tag: Tag) extends Table[User](tag, "USERS") {
  def id = column[Long]("ID", O.PrimaryKey, O.AutoInc)

  def firstName = column[String]("FIRST_NAME")

  def lastName = column[String]("LAST_NAME")

  def email = column[String]("EMAIL")

  def password = column[String]("PASSWORD")

  override def * =
    (id, firstName, lastName, email, password)  <> ((User.apply _).tupled, User.unapply)
}

@Singleton
class Users @Inject()(protected val dbConfigProvider: DatabaseConfigProvider) extends HasDatabaseConfigProvider[JdbcProfile] {

  val users = TableQuery[UserTableDef]

  def add(user: User): Future[String] = {
    db.run(users += user).map(_ => "Success")
  }

  def delete(id: Long): Future[String] = {
    db.run(users.filter(_.id === id).delete).map(_ => "")
  }

  def update(user: User): Future[String] = {
    db.run(users.insertOrUpdate(user)).map(_ => "Success")
  }

  def get(id: Long): Future[Option[User]] = {
    db.run(users.filter(_.id === id).result.headOption)
  }

  def get(email: String, password: String): Future[Option[User]] = {
    db.run(users.filter(value => value.email === email && value.password === password).result.headOption)
  }

  def get(email: String): Future[Option[User]] = {
    db.run(users.filter(_.email === email).result.headOption)
  }

  def listAll: Future[Seq[User]] = {
    db.run(users.result)
  }

}